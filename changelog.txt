changelog of the Apifun Scilab Toolbox

apifun (not released yet.)
    * Fixed bug 898, again, and again!

apifun (0.4)
    * Clarified help of apifun_expandfromsize
	* Fixed bug in the examples of apifun_keyvaluepairs.
	* Fixed bug #826
	  apifun_expandfromsize could not manage 3 arguments.
	  http://forge.scilab.org/index.php/p/apifun/issues/826
	* Fixed bug #605
	  The example in the help page of apifun_checkflint was wrong.
	  http://forge.scilab.org/index.php/p/apifun/issues/605
	* Fixed bug #606
	  The error message in apifun_checkflint was wrong.
	  Also fixed checkvector, checkvecrow and checkveccol.
	  http://forge.scilab.org/index.php/p/apifun/issues/606
	* Fixed bug #633
	  apifun_checkgreq may produce a wrong error message.
	  Same in apifun_checkloweq.
	  http://forge.scilab.org/index.php/p/apifun/issues/633/
	* Fixed bug #703
	  apifun_checkgreq could not manage empty matrices.
	  Same in apifun_checkloweq.
	  http://forge.scilab.org/index.php/p/apifun/issues/703/
	* Fixed bug #741
	  apifun_checkoption failed if var was not a scalar
	  http://forge.scilab.org/index.php/p/apifun/issues/741/
	* Fixed bug #865
	  The help page of apifun_expandvar was unclear.
	  http://forge.scilab.org/index.php/p/apifun/issues/865/
	* Fixed bug #898
	  apifun_checkrange did not take input matrices.
	  http://forge.scilab.org/index.php/p/apifun/issues/898/

apifun (0.3)
    * Removed warnings in 2 unit tests.
    * Created apifun_keyvaluepairs (fixed bug #539).
    * Fixed bug #540: 
      assert_typecallable did not manage compiled functions.
    * Clarified help of apifun_expandvar.
    * Added message on apifun_overview at startup.
    * Created apifun_expandfromsize (fixed bug #334).
    * Created apifun_checkreal (bug #233).
      Created apifun_checkcomplex.
    * Organized help pages into sections.

apifun (0.2)
    * Removed unnecessary commas.
    * Fixed bug #480 in the name of the library 
      http://forge.scilab.org/index.php/p/apifun/issues/480/
    * Added warning if already loaded.
    * Added help cleaner.
    * Removed auto-generated demos.
    * Fixed bug #129: created apifun_argindefault 
      http://forge.scilab.org/index.php/p/apifun/issues/129/
    * Created unit tests for checkvector
    * Created unit tests for checkveccol
    * Created unit tests for checkvecrow
    * Fixed bug #153 
      http://forge.scilab.org/index.php/p/apifun/issues/153/
        * Update apifun_checkvector to make the nbval optionnal
        * Update apifun_checkveccol to make the nbval optionnal
        * Update apifun_checkvecrow to make the nbval optionnal
    * Created unit test for checkrange.
    * Created unit test for checkoption.
    * Created unit test for checkgreq.
    * Created unit test for checkloweq.
    * Created unit test for checksquare.
    * Fixed bug #232: The apifun_checkrange function did not manage NANs.
      http://forge.scilab.org/index.php/p/apifun/issues/232/
    * Fixed bug #327: The expandvar function has been added to apifun.
      http://forge.scilab.org/index.php/p/apifun/issues/327/
    * Fixed bug #142: Add a apifun_checkcallable function
      http://forge.scilab.org/index.php/p/apifun/issues/142/
    * Fixed bug #141: Added a apifun_checkflint function.
      http://forge.scilab.org/index.php/p/apifun/issues/141/
    * Fixed internal error messages, when type of input arguments 
      given to apifun_* functions is wrong.
 -- Michael Baudin <michael.baudin@scilab.org>  July 2011

apifun (0.1)
    * Initial version
 -- Michael Baudin <michael.baudin@scilab.org>  July 2010

