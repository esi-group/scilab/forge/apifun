// Copyright (C) 2010 - DIGITEO - Michael Baudin

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.

//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating apifun\n");
helpdir = cwd;
funmat = [
  "apifun_checkrhs"
  "apifun_checklhs"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "apifun";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
mprintf("Updating apifun/checktype\n");
helpdir = fullfile(cwd,"checktype");
funmat = [
  "apifun_checkcallable"
  "apifun_checktype"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "apifun";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
mprintf("Updating apifun/checkcontent\n");
helpdir = fullfile(cwd,"checkcontent");
funmat = [
  "apifun_checkoption"
  "apifun_checkgreq"
  "apifun_checkloweq"
  "apifun_checkrange"
  "apifun_checkflint"
  "apifun_checkreal"
  "apifun_checkcomplex"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "apifun";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
mprintf("Updating apifun/checksize\n");
helpdir = fullfile(cwd,"checksize");
funmat = [
  "apifun_checkdims"
  "apifun_checksquare"
  "apifun_checkvector"
  "apifun_checkvecrow"
  "apifun_checkveccol"
  "apifun_checkscalar"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "apifun";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

//
mprintf("Updating apifun/support\n");
helpdir = fullfile(cwd,"Support");
funmat = [
  "apifun_argindefault"
  "apifun_expandvar"
  "apifun_expandfromsize"
  "apifun_keyvaluepairs"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "apifun";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );

