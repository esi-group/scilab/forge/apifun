<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from apifun_argindefault.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="apifun_argindefault" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>apifun_argindefault</refname><refpurpose>Returns the value of an input argument.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   argin=apifun_argindefault(vararglist,ivar,default)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>vararglist :</term>
      <listitem><para> a list, the arguments in the calling sequence</para></listitem></varlistentry>
   <varlistentry><term>ivar :</term>
      <listitem><para> a 1-by-1 matrix of floating point integers, the index of the input argument in the calling sequence</para></listitem></varlistentry>
   <varlistentry><term>default :</term>
      <listitem><para> the default value for the argument, if the argument is not provided or if the argument is the empty matrix</para></listitem></varlistentry>
   <varlistentry><term>argin :</term>
      <listitem><para> the actual value of the argument</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Returns the value of the input argument #ivar, from the list of arguments in vararglist.
If this argument was not provided (i.e. if the length of vararglist is lower than ivar),
or if the element at index #ivar in the list is equal to the
empty matrix, returns the default value.
   </para>
   <para>
This function is designed to be used in function which provide an
optional number of input arguments.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Case where the argument is there
vararglist = list("a","b","c");
ivar = 2;
default = "e";
argin=apifun_argindefault ( vararglist,ivar,default ) // "b"

// Case where argument is not there
vararglist = list("a","b","c");
ivar = 4;
default = "e";
argin=apifun_argindefault ( vararglist,ivar,default ) // "e"

// Case where argument is there, but is empty matrix
vararglist = list([],"b","c");
ivar = 1;
default = "e";
argin=apifun_argindefault ( vararglist,ivar,default );
assert_equal ( argin,"e" );

// A practical use-case: a function with 3 optional arguments.
function y = myfun ( varargin )
//   y = myfun(x)
//   y = myfun(x,a)
//   y = myfun(x,a,b)
//   y = myfun(x,a,b,c)
//
// Returns y = a*x^b+c
// Defaults are a=5, b=6, c=7.
// If any optional argument is [], we use the default value.
[lhs, rhs] = argn();
if ( rhs < 1 | rhs > 4 ) then
errmsg = msprintf(gettext("%s: Wrong number of input arguments: %d to %d expected.\n"), "myfun", 1,4);
error(errmsg)
end
//
x = varargin(1)
a=apifun_argindefault ( varargin,2,5 )
b=apifun_argindefault ( varargin,3,6 )
c=apifun_argindefault ( varargin,4,7 )
y = a.*x.^b + c
endfunction
//
y = myfun(7) // 5*7^6+7
y = myfun(7,2) // 2*7^6+7
y = myfun(7,2,3) // 2*7^3+7
y = myfun(7,2,3,4) // 2*7^3+4
y = myfun(7,[],3,4) // 5*7^3+4
y = myfun(7,[],[],4) // 5*7^6+4
y = myfun(7,[],[],[]) // 5*7^6+7

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Michael Baudin - 2010-2011 - DIGITEO</member>
   </simplelist>
</refsection>
</refentry>
