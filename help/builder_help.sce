// ====================================================================
// Copyright (C) 2012 - Michael Baudin
// Copyright INRIA 2008
// This file is released into the public domain
// ====================================================================
function apifunBuildHelp()
    help_dir = get_absolute_file_path('builder_help.sce');
    tbx_builder_help_lang("en_US", help_dir);
endfunction
apifunBuildHelp()
clear apifunBuildHelp
